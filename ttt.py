board = [' ' for x in range(1,11)]
player_count = computer_count = draw_count = 0
history_of_moves = {}
def insert_letter(letter, pos):
    board[pos] = letter
def free_space(pos):
    return board[pos] == ' '
def display_board(board):
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9] + '\n-----------\n' + ' ' + board[4] + ' | ' + board[5] + ' | ' + board[6] + '\n-----------\n' + ' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
def winner(bo, le):
    return (bo[7] == bo[8] == bo[9] == le) or (bo[4] == bo[5] == bo[6] == le) or (bo[1] == bo[2] == bo[3] == le) or (bo[1] == bo[4] == bo[7] == le) or (
            bo[2] == bo[5] == bo[8] == le) or (bo[3] == bo[6] == bo[9] == le) or (bo[1] == bo[5] == bo[9] == le) or (bo[3] == bo[5] == bo[7] == le)
def player_move():
    run = True
    acceptable_range = range(1, 10)
    while run :
        move = input('Please select a position to place an \'X\' (1-9): ')
        try:
            move = int(move)
            if move in acceptable_range:
                if free_space(move):
                    run = False
                    insert_letter('X', move)
                else:
                    print('Sorry, this space is occupied!')
            else:
                print('Please type a number within the range!')
        except:
            print('Please type a number!')
def computer_move():
    possible_moves = [x for x, letter in enumerate(board) if letter == ' ' and x != 0]
    move = 0
    for let in ['O', 'X']:
        for i in possible_moves:
            board_copy = board[:]
            board_copy[i] = let
            if winner(board_copy, let):
                move = i
                return move
    corners = []
    for i in possible_moves:
        if i in [1, 3, 7, 9]:
            corners.append(i)
    if len(corners) > 0:
        move = selectRandom(corners)
        return move
    if 5 in possible_moves:
        move = 5
        return move
    edges = []
    for i in possible_moves:
        if i in [2, 4, 6, 8]:
            edges.append(i)
    if len(edges) > 0:
        move = selectRandom(edges)
    return move
def selectRandom(li):
    import random
    ln = len(li)
    r = random.randrange(0, ln)
    return li[r]
def board_full(board):
    if board.count(' ') > 1:
        return False
    else:
        return True
print('Welcome to Tic Tac Toe!')
for round in range(1,11):
    print("     ROUND {}\n".format(round))
    display_board(board)
    while not (board_full(board)):
        if not (winner(board, 'O')):
            player_move()
            display_board(board)
        else:
            print('Computer wins this round!\n')
            result = 'COMPUTER WINS'
            computer_count+=1
            break
        if not (winner(board, 'X')):
            move = computer_move()
            if move == 0:
                print('DRAW!\n')
                result = 'DRAW'
                draw_count+=1
            else:
                insert_letter('O', move)
                print('Computer placed an \'O\' in position', move, ':')
                display_board(board)
        else:
            print('Player wins this round!\n')
            result = 'PLAYER WINS'
            player_count+=1
            break
    history_of_moves[str(round)] = [board,result]
    board = [' ' for x in range(1,11)]
if player_count > computer_count:
    print("PLAYER IS THE WINNER OF THE GAME!\n\n")
elif computer_count > player_count:
    print("COMPUTER IS THE WINNER OF THE GAME!\n\n")
else:
    print("The game has ended in a draw!\n\n")
while True:
    ch = int(input("Enter the round for which you need the information:"))
    print("Game Result: {}\n".format(history_of_moves[str(ch)][1])+ "\n".format(display_board(history_of_moves[str(ch)][0])))
    check_again = input('Would you like to check the moves made for another round [y/n]? ')
    if check_again.lower() == 'n':
        break